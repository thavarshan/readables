<?php

use Illuminate\Support\Facades\Route;

Route::middleware('guest')->get('/', function () {
    return redirect()->route('login');
});

Auth::routes([
    'register' => false,
    'reset' => false
]);

Route::group([
    'middleware' => 'auth'
], function () {
    Route::get('/home/{category?}', 'HomeController')->name('home');

    Route::resource('/books', 'BookController', [
        'except' => ['index', 'show']
    ]);

    Route::get('/books/{book}', 'BookController@destroy')->name('book.destroy');

    Route::resource('/categoies', 'CategoryController', [
        'except' => ['show', 'create']
    ]);
});
