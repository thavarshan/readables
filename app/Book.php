<?php

namespace App;

use App\Traits\Fillable;
use App\Traits\Filterable;
use App\Traits\Searchable;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use Fillable, Filterable, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'author', 'price', 'medium', 'image',
        'isbn', 'published_at', 'category_id', 'description'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'published_at' => 'date:Y-m-d',
    ];

    /**
     * The relationships to always eager-load.
     *
     * @var array
     */
    protected $with = ['category'];

    /**
     * Get the books cover image.
     *
     * @param string $value
     *
     * @return string
     */
    public function getImageAttribute($value)
    {
        if (Str::contains($value, 'http')) {
            return $value;
        }

        return asset($value ?: 'img/book.png');
    }

    /**
     * Set the books's price in cents.
     *
     * @param string $value
     * @return string
     */
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }

    /**
     * Get the books's price.
     *
     * @param  string  $value
     * @return string
     */
    public function getPriceAttribute($value)
    {
        return $value / 100;
    }

    /**
     * Get the category details this book belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
