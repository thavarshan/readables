<?php

namespace App\Traits;

trait Searchable
{
    /**
     * Filter datatbase query according to given parameters.
     *
     * @param  string $query
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function search($query)
    {
        $builder = static::latest();

        return is_null($query)
            ? $builder
            : $builder->where('name', 'like', '%'.$query.'%')
                      ->orWhere('isbn', 'like', '%'.$query.'%')
                      ->orWhere('author', 'like', '%'.$query.'%')
                      ->orWhere('price', 'like', '%'.$query.'%');
    }
}
