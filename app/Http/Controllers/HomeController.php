<?php

namespace App\Http\Controllers;

use App\Category;
use App\Filters\BookFilters;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\Filterable;

class HomeController extends Controller
{
    use Filterable;

    /**
     * Show the application dashboard.
     *
     * @param \App\Category
     * @param \Illuminate\Http\Request
     * @param \App\Filters\BookFilters
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke(Category $category, Request $request, BookFilters $filter)
    {
        return view('home', $this->getBooks($category, $request->search, $filter));
    }
}
