<?php

namespace App\Http\Controllers\Traits;

use App\Book;
use App\Category;
use App\Filters\BookFilters;

trait Filterable
{
    /**
     * Fetch all relevant books.
     *
     * @param \App\Category $category
     * @param string       $query
     * @param \App\Filters\BookFilters $filter
     * @return array
     */
    protected function getBooks(Category $category, $query = null, BookFilters $filter)
    {
        $books = Book::search($query)->with('category')->filter($filter);

        if ($category->exists) {
            $books->where('category_id', $category->id);
        }

        return [
            'categories' => Category::all(),
            'books' => $books->paginate(12)
        ];
    }
}
