<?php

namespace App\Http\Controllers;

use App\Book;
use App\Category;
use App\Http\Requests\Book as BookForm;

class BookController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.create', [
            'book' => new Book,
            'categories' => Category::all()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        return view('books.edit', [
            'book' => $book,
            'categories' => Category::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Book  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookForm $request)
    {
        Book::create($this->attributes($request));

        return success(route('home'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Book  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(BookForm $request, Book $book)
    {
        $book->update($this->attributes($request));

        return success(route('home'), 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();

        return success(route('home'), 'deleted');
    }

    /**
     * Get only mass assignable attributes from request object.
     *
     * @param  \App\Http\Requests\Book $request
     * @return array
     */
    protected function attributes($request)
    {
        if (is_null($request->image)) {
            return $request->except('image');
        }

        return array_merge(
            $request->only(Book::attributes()),
            ['image' => $request->file('image')->store('img', 'public')]
        );
    }
}
