<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class Book extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'author' => ['required', 'string', 'max:255'],
            'price' => ['required', 'integer'],
            'medium' => ['required', 'string', 'max:255'],
            'image' => $this->hasImage(),
            'isbn' => ['required', 'integer'],
            'published_at' => ['required', 'date'],
            'category_id' => ['required', 'integer'],
        ];
    }

    /**
     * Determine if new image has been uploaded.
     *
     * @return boolean
     */
    protected function hasImage()
    {
        return $this->image
            ? ['required', 'image']
            : ['nullable'];
    }
}
