<?php

namespace App\Filters;

use App\Models\Company;

class BookFilters extends Filter
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = ['author', 'published', 'price'];

    /**
     * Filter the query by a given company name.
     *
     * @param  string $slug
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function author($name)
    {
        return $this->builder->where('author', $name);
    }
}
