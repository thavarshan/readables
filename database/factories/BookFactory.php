<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence(2) ,
        'author' => $faker->name,
        'price' => rand(1, 9) * 100,
        'medium' => 'English',
        'image' => 'img/book.png',
        'isbn' => uniqid(),
        'published_at' => now()->subMonths(rand(12, 100)),
        'category_id' => rand(1, 7),
        'description' => $faker->paragraph(4)
    ];
});
