<?php

use App\Book;
use App\Category;
use Illuminate\Database\Seeder;

class DummyDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        create(Category::class, [], 10)->each(function ($category) {
            create(Book::class, ['category_id' => $category->id], 20);
        });
    }
}
