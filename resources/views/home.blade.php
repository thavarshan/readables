@extends('layouts.app')

@section('content')
<section class="pt-12 pb-40 bg-gray-800">
    <div class="container">
        <div class="lg:flex lg:items-center lg:justify-between">
            <div class="flex-1 min-w-0">
                <h2 class="text-2xl font-bold leading-7 text-white sm:text-3xl sm:leading-9  ">
                    Dashboard
                </h2>

                <div class="mt-1 flex flex-col sm:mt-0 sm:flex-row sm:flex-wrap max-w-sm">
                    <div class="mt-2 flex items-center text-gray-500 font-semibold sm:mr-6">
                        Welcome, {{ user('name') }}
                    </div>
                </div>
            </div>

            <div class="mt-5 flex lg:mt-0 lg:ml-4">
                <a href="{{ route('books.create') }}" class="btn hover:text-white focus:text-white">
                    New book
                </a>
            </div>
        </div>
    </div>
</section>

<section class="py-12 -mt-40">
    <div class="container">
        <div class="row justify-center">
            <div class="col-12">
                <div class="overflow-hidden">
                    <div class="inline-block min-w-full shadow rounded-lg overflow-auto">
                        <div class="px-5 py-3 bg-white flex items-center justify-between">
                            <div class="dropdown mr-64">
                                <button class="btn bg-gray-100 text-gray-800 hover:text-gray-800 hover:bg-gray-200 focus:text-gray-800 focus:bg-gray-200 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ request()->category->name ?? 'All categories' }}
                                </button>

                                <div class="dropdown-menu dropdown-menu-right border-none rounded-lg shadow-lg" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item text-sm font-medium" href="{{ route('home') }}">All categories</a>

                                    @foreach ($categories as $category)
                                        <a class="dropdown-item text-sm font-medium" href="{{ route('home', ['category' => $category->slug]) }}">{{ $category->name }}</a>
                                    @endforeach
                                </div>
                            </div>

                            <div class="w-2/5">
                                <form class="w-full flex items-center justify-between" method="GET" action="{{ route('home') }}">
                                    <div class="relative flex-1">
                                        <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                            <svg class="h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor">
                                                <path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path>
                                            </svg>
                                        </div>

                                        <input type="search" id="search" name="search" class="form-input w-full pl-10" placeholder="Type and hit enter to search..." value="{{ old('search') ?? request()->search }}">

                                        @if (! is_null(request()->search))
                                            <div class="absolute inset-y-0 right-0 pr-3 flex items-center">
                                                <a href="{{ route('home') }}" class="ml-3 btn bg-transparent text-gray-800 hover:text-gray-800 hover:bg-transparent focus:text-gray-800 focus:bg-transparent px-0">
                                                    <svg fill="none" class="h-4 w-4" viewBox="0 0 24 24" stroke="currentColor">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                                                    </svg>
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>

                        <table class="min-w-full leading-normal">
                            <thead>
                                <tr>
                                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Book / Title
                                </th>

                                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Author
                                </th>

                                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Price
                                </th>

                                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                    Published
                                </th>

                                <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100"></th>
                                </tr>
                            </thead>

                            <tbody>
                                @forelse ($books as $book)
                                    <tr>
                                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <div class="flex items-center">
                                                <div class="flex-shrink-0 w-10">
                                                    <img class="w-full h-auto" src="{{ asset($book->image ?? 'img/cover.jpg') }}" alt="{{ $book->name }}">
                                                </div>

                                                <div class="ml-3">
                                                    <p class="text-gray-800 font-semibold whitespace-no-wrap">
                                                        {{ $book->name }}
                                                    </p>

                                                    <p class="text-gray-600 whitespace-no-wrap">
                                                        {{ $book->isbn }}
                                                    </p>
                                                </div>
                                            </div>
                                        </td>

                                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p class="whitespace-no-wrap">
                                                <a class="text-gray-700 hover:text-indigo-600" href="{{ route('home', ['author' => $book->author]) }}">
                                                    {{ $book->author }}
                                                </a>
                                            </p>
                                        </td>

                                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p class="text-gray-900 whitespace-no-wrap">{{ '$' . $book->price  }}</p>
                                            <p class="text-gray-600 whitespace-no-wrap">USD</p>
                                        </td>

                                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p class="text-gray-900 whitespace-no-wrap">{{ $book->published_at->format('M j, Y') }}</p>
                                            <p class="text-gray-600 whitespace-no-wrap">{{ $book->published_at->diffForHumans() }}</p>
                                        </td>

                                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm text-right">
                                            <div class="dropdown">
                                                <button class="dropdown-toggle inline-block text-gray-500 hover:text-gray-700" id="bookDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <svg class="inline-block h-6 w-6 fill-current" viewBox="0 0 24 24">
                                                        <path d="M12 6a2 2 0 110-4 2 2 0 010 4zm0 8a2 2 0 110-4 2 2 0 010 4zm-2 6a2 2 0 104 0 2 2 0 00-4 0z"></path>
                                                    </svg>
                                                </button>

                                                <div class="dropdown-menu dropdown-menu-right w-48 rounded-lg shadow-lg mt-2 border-none"  aria-labelledby="bookDropdown">
                                                    <a href="#" class="whitespace-no-wrap block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-700 focus:text-gray-700 transition ease-in-out duration-150" data-toggle="modal" data-target="#viewBookModal{{ $book->id }}" role="button">View</a>

                                                    <a href="{{ route('books.edit', $book) }}" class="whitespace-no-wrap block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-700 focus:text-gray-700 transition ease-in-out duration-150">Edit</a>

                                                    <a href="#" class="whitespace-no-wrap block px-4 py-2 text-sm text-red-700 hover:bg-gray-100 hover:text-red-700 focus:text-red-700 transition ease-in-out duration-150" data-toggle="modal" data-target="#deleteModal{{ $book->id }}">Delete</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="viewBookModal{{ $book->id }}" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="viewBookModalLabel{{ $book->id }}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered rounded-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header pl-6">
                                                    <div class="modal-title">
                                                        <h3 class="text-lg modal-title leading-6 font-medium text-gray-900" id="viewBookModalLabel{{ $book->id }}">
                                                            {{ $book->name }}
                                                        </h3>

                                                        <div class="text-indigo-500 text-sm">{{ $book->isbn }}</div>
                                                    </div>

                                                    <button type="button" class="close font-normal" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                                <div class="modal-body px-6 border-none">
                                                    <div class="row">
                                                        <div class="col-md-6 mb-8">
                                                            <div class="text-gray-500 text-sm">Author</div>
                                                            <div>{{ $book->author }}</div>
                                                        </div>

                                                        <div class="col-md-6 mb-8">
                                                            <div class="text-gray-500 text-sm">Published</div>
                                                            <div>{{ $book->published_at->format('M j, Y') }}</div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6 mb-8">
                                                            <div class="text-gray-500 text-sm">Medium</div>
                                                            <div>{{ $book->medium }}</div>
                                                        </div>

                                                        <div class="col-md-6 mb-8">
                                                            <div class="text-gray-500 text-sm">Price</div>
                                                            <div>{{ '$' . $book->price / 100 }}</div>
                                                        </div>
                                                    </div>

                                                    <div class="row mb-8">
                                                        <div class="col-12">
                                                            <div class="text-gray-500 text-sm">Description</div>
                                                            <div>{{ $book->description }}</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="deleteModal{{ $book->id }}" tabindex="-1" role="dialog" aria-labelledby="deleteModalTitle{{ $book->id }}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content border-none rounded-lg">
                                                <div class="modal-header pl-6 border-none">
                                                    <h3 class="text-lg modal-title leading-6 font-medium text-gray-900" id="deleteModalTitle{{ $book->id }}">
                                                        Delete Book
                                                    </h3>

                                                    <button type="button" class="close font-normal" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                                <div class="modal-body px-6 pb-6">
                                                    <p class="text-sm leading-5 text-gray-600">
                                                        Are you sure you want to delete the book <span class="font-medium text-gray-700">{{ $book->name }}</span>? All of its data will be permanantly removed. This action cannot be undone.
                                                    </p>
                                                </div>

                                                <div class="modal-footer border-none bg-gray-100 pr-6">
                                                    <button type="button" class="btn bg-white text-gray-800 hover:bg-indigo-100 hover:text-gray-800 focus:bg-indigo-100 focus:text-gray-800" data-dismiss="modal">Cancel</button>

                                                    <a href="{{ route('books.destroy', $book) }}" class="btn hover:text-white focus:text-white bg-red-500 hover:bg-red-400">Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <tr>
                                        <div class="text-gray-600">
                                            There are no book available.
                                        </div>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="flex items-end justify-between">
                    <div>
                        <span class="text-gray-600">
                            Showing {{ $books->firstItem() }} to {{ $books->lastItem() }} of {{ $books->total() }}
                        </span>
                    </div>

                    <div>
                        {{ $books->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="newBook" data-backdrop="static" aria-labelledby="newBookLabel" aria-hidden="true" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-center" role="document">
        <form class="modal-content border-none rounded-lg" action="{{ route('books.store') }}" method="POST">
            @csrf

            <div class="modal-header border-none pl-6">
                <h5 class="modal-title font-semibold text-xl" id="newBookLabel">Add New Book</h5>

                <button type="button" class="close font-normal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body px-6">
                <div class="row">
                    <div class="col-md-6 mb-6">
                        <label for="name" class="block">
                            <span class="text-sm mb-2 font-semibold">{{ __('Name of book') }}</span>

                            <input id="name" type="text" class="form-input mt-1 block w-full @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Ex. Everything I Never Told You">
                        </label>

                        @error('name')
                            <span class="text-sm block mt-2 text-red-500" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6 mb-6">
                        <label for="image">
                            <span class="text-sm mb-2 font-semibold">{{ __('Image') }}</span>

                            <input type="file" class="w-full block mt-1" name="image" id="image">
                        </label>

                        @error('image')
                            <span class="text-sm block mt-2 text-red-500" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-6">
                        <label for="author" class="block">
                            <span class="text-sm mb-2 font-semibold">{{ __('Author') }}</span>

                            <input id="author" type="text" class="form-input mt-1 block w-full @error('author') is-invalid @enderror" name="author" value="{{ old('author') }}" required autocomplete="author" placeholder="Celeste Ng">
                        </label>

                        @error('author')
                            <span class="text-sm block mt-2 text-red-500" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6 mb-6">
                        <label for="price" class="block">
                            <span class="text-sm mb-2 font-semibold">{{ __('Price ($)') }}</span>

                            <input id="price" type="text" class="form-input mt-1 block w-full @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}" required autocomplete="price" placeholder="6.99">
                        </label>

                        @error('price')
                            <span class="text-sm block mt-2 text-red-500" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-6">
                        <label class="block">
                            <span class="text-sm mb-2 font-semibold">{{ __('Medium') }}</span>

                            <select name="medium" id="medium" class="form-select mt-1 block w-full">
                                <option value="Sinhala" selected>Sinhala</option>
                                <option value="English">English</option>
                                <option value="Tamil">Tamil</option>
                            </select>
                        </label>

                        @error('medium')
                            <span class="text-sm block mt-2 text-red-500" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6 mb-6">
                        <label for="isbn" class="block">
                            <span class="text-sm mb-2 font-semibold">{{ __('ISBN') }}</span>

                            <input id="isbn" type="text" class="form-input mt-1 block w-full @error('isbn') is-invalid @enderror" name="isbn" value="{{ old('isbn') }}" required autocomplete="isbn" placeholder="9783161484100">
                        </label>

                        @error('isbn')
                            <span class="text-sm block mt-2 text-red-500" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-6">
                        <label for="published_at" class="block">
                            <span class="text-sm mb-2 font-semibold">{{ __('Published on') }}</span>

                            <input id="published_at" type="text" class="form-input date_picker mt-1 block w-full @error('published_at') is-invalid @enderror" name="published_at" value="{{ old('published_at') }}" required autocomplete="published_at" placeholder="{{ now()->format('M j, Y') }}">
                        </label>

                        @error('published_at')
                            <span class="text-sm block mt-2 text-red-500" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>

                    <div class="col-md-6 mb-6">
                        <label class="block">
                            <span class="text-sm mb-2 font-semibold">{{ __('Category') }}</span>

                            <select name="category_id" id="category_id" class="form-select mt-1 block w-full">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </label>

                        @error('category_id')
                            <span class="text-sm block mt-2 text-red-500" role="alert">
                                {{ $message }}
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="modal-footer border-none bg-gray-100 pr-6">
                <button type="button" class="btn bg-white text-gray-800 hover:bg-indigo-100 hover:text-gray-800 focus:bg-indigo-100 focus:text-gray-800" data-dismiss="modal">Cancel</button>

                <button type="submit" class="btn">Add new</button>
            </div>
        </form>
    </div>
</div>
@endsection
