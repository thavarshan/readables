<nav>
    <div class="container">
        <div class="max-w-7xl mx-auto">
            <div class="border-b border-gray-700">
                <div class="flex items-center justify-between h-16 px-4 sm:px-0">
                    <div class="flex items-center">
                        <div class="flex-shrink-0">
                            <a href="/" class="block h-5">
                                <img class="h-5 w-auto" src="{{ asset('img/logo.png') }}" alt="{{ config('app.name') }}">
                            </a>
                        </div>
                    </div>

                    <div class="hidden md:block">
                        <div class="ml-4 flex items-center md:ml-6">
                            @auth
                                <div class="dropdown ml-6 relative">
                                    <button class="dropdown-toggle max-w-xs flex items-center text-sm rounded-full focus:outline-none focus:shadow-solid-white transition ease-in-out duration-150" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img class="h-8 w-8 rounded-full" src="{{ asset('img/avatars/default.svg') }}" alt="{{ user('username') }}">
                                    </button>

                                    <div class="dropdown-menu dropdown-menu-right w-48 rounded-lg shadow-lg mt-2 border-none"  aria-labelledby="dropdownMenuLink">
                                        <a href="#" class="whitespace-no-wrap block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-700 focus:text-gray-700 transition ease-in-out duration-150">Profile</a>

                                        <a href="#" class="whitespace-no-wrap block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-700 focus:text-gray-700 transition ease-in-out duration-150">Settings</a>

                                        <a class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 hover:text-gray-700 focus:text-gray-700 transition ease-in-out duration-150" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ __('Sign out') }}</a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            @else
                                <a href="{{ route('login') }}" class="{{ is_active('login', 'bg-gray-900') }} ml-4 px-3 py-2 rounded-lg text-sm font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">Sign in</a>

                                <a href="{{ route('register') }}" class="{{ is_active('register', 'bg-gray-900') }} ml-4 px-3 py-2 rounded-lg text-sm font-medium text-white hover:text-white bg-indigo-500 hover:bg-indigo-400 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">Create account</a>
                            @endauth
                        </div>
                    </div>

                    <div class="-mr-2 flex md:hidden">
                        <button class="navbar-toggler inline-flex items-center justify-center p-2 rounded-lg text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:bg-gray-700 focus:text-white" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                            <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                                <path class="inline-flex" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16"></path>
                                <path class="hidden" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="border-b border-gray-700 collapse" id="navbarToggleExternalContent">
            <div class="px-2 py-3 sm:px-3 hidden">
                <a href="#" class="whitespace-no-wrap block px-3 py-2 rounded-lg text-base font-medium focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">Dashboard</a>

                @guest
                    <a href="{{ route('login') }}" class="whitespace-no-wrap mt-1 block px-3 py-2 rounded-lg text-base font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">Sign in</a>

                    <a href="{{ route('register') }}" class="whitespace-no-wrap mt-1 block px-3 py-2 rounded-lg text-base font-medium text-gray-300 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">Create account</a>
                @endguest
            </div>

            @auth
                <div class="pt-4 pb-3 border-t border-gray-700">
                    <div class="flex items-center px-5 sm:px-6">
                        <div class="flex-shrink-0">
                            <img class="h-10 w-10 rounded-full" src="{{ asset('img/avatars/default.svg') }}" alt="{{ user('username') }}">
                        </div>

                        <div class="ml-3">
                            <div class="text-base font-medium leading-none text-white">{{ user('name') }}</div>

                            <div class="mt-1 text-sm font-medium leading-none text-gray-400"><a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="5e2a31331e3f2e2e323b70303b2a">{{ user('email') }}</a></div>
                        </div>
                    </div>

                    <div class="mt-3 px-2 sm:px-3">
                        <a href="#" class="whitespace-no-wrap block px-3 py-2 rounded-lg text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">Profile</a>

                        <a href="#" class="whitespace-no-wrap mt-1 block px-3 py-2 rounded-lg text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">Settings</a>

                        <a href="#" class="whitespace-no-wrap mt-1 block px-3 py-2 rounded-lg text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:text-white focus:bg-gray-700 transition ease-in-out duration-150">Sign out</a>
                    </div>
                </div>
            @endauth
        </div>
    </div>
</nav>
