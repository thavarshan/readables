@extends('layouts.app')

@section('content')
    <section class="py-12 bg-gray-800">
        <div class="container">
            <div class="lg:flex lg:items-center lg:justify-between">
                <div class="flex-1 min-w-0">
                    <h2 class="text-2xl font-bold leading-7 text-white sm:text-3xl sm:leading-9">
                        Add New Book
                    </h2>

                    <div class="mt-1 flex flex-col sm:mt-0 sm:flex-row sm:flex-wrap">
                        <div class="mt-2 flex items-center text-gray-500 font-semibold sm:mr-6">
                            Filling in the all the information will create a new book entry in the database.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <form action="{{ route('books.store') }}" method="POST" class="py-12" enctype="multipart/form-data">
        @csrf

        <div class="container">
            <div class="row">
                <div class="col-md-4 mb-6 md:mb-0">
                    <h4 class="text-xl font-semibold">
                        Book Details
                    </h4>

                    <p class="text-sm text-gray-500 max-w-sm">
                        This information will be displayed publicly to the customer and will be used for sorting.
                    </p>
                </div>

                <div class="col-md-8 mb-6 md:mb-0">
                    <div class="bg-white px-4 py-5 sm:px-6 rounded-lg shadow mb-6">
                        <div class="row">
                            <div class="col-md-6 mb-6">
                                <label for="name" class="block">
                                    <span class="text-sm mb-2 font-semibold">{{ __('Name of book') }}</span>

                                    <input id="name" type="text" class="form-input mt-1 block w-full @error('name') is-invalid @enderror" name="name" value="{{ old('name') ?? ($book->name ?? null) }}" required autocomplete="name" placeholder="Ex. Everything I Never Told You">
                                </label>

                                @error('name')
                                    <span class="text-sm block mt-2 text-red-500" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-6">
                                <label for="image">
                                    <span class="text-sm mb-2 font-semibold">{{ __('Image') }}</span>

                                    <input type="file" class="w-full block mt-1" name="image" id="image">
                                </label>

                                @error('image')
                                    <span class="text-sm block mt-2 text-red-500" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-6">
                                <label for="author" class="block">
                                    <span class="text-sm mb-2 font-semibold">{{ __('Author') }}</span>

                                    <input id="author" type="text" class="form-input mt-1 block w-full @error('author') is-invalid @enderror" name="author" value="{{ old('author') ?? ($book->author ?? null) }}" required autocomplete="author" placeholder="Celeste Ng">
                                </label>

                                @error('author')
                                    <span class="text-sm block mt-2 text-red-500" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-6">
                                <label for="price" class="block">
                                    <span class="text-sm mb-2 font-semibold">{{ __('Price ($)') }}</span>

                                    <input id="price" type="text" class="form-input mt-1 block w-full @error('price') is-invalid @enderror" name="price" value="{{ old('price') ?? ($book->price / 100 ?? null) }}" required autocomplete="price" placeholder="6.99">
                                </label>

                                @error('price')
                                    <span class="text-sm block mt-2 text-red-500" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-6">
                                <label class="block">
                                    <span class="text-sm mb-2 font-semibold">{{ __('Medium') }}</span>

                                    <select name="medium" id="medium" class="form-select mt-1 block w-full">
                                        <option value="Sinhala" selected>Sinhala</option>
                                        <option value="English">English</option>
                                        <option value="Tamil">Tamil</option>
                                    </select>
                                </label>

                                @error('medium')
                                    <span class="text-sm block mt-2 text-red-500" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-6">
                                <label for="isbn" class="block">
                                    <span class="text-sm mb-2 font-semibold">{{ __('ISBN') }}</span>

                                    <input id="isbn" type="text" class="form-input mt-1 block w-full @error('isbn') is-invalid @enderror" name="isbn" value="{{ old('isbn') ?? ($book->isbn ?? null) }}" required autocomplete="isbn" placeholder="9783161484100">
                                </label>

                                @error('isbn')
                                    <span class="text-sm block mt-2 text-red-500" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-6">
                                <label for="published_at" class="block">
                                    <span class="text-sm mb-2 font-semibold">{{ __('Published on') }}</span>

                                    <input id="published_at" type="text" min="13" class="form-input date_picker mt-1 block w-full @error('published_at') is-invalid @enderror" name="published_at" value="{{ old('published_at') ?? ($book->published_at ?? null) }}" required autocomplete="published_at" placeholder="{{ now()->format('M j, Y') }}">
                                </label>

                                @error('published_at')
                                    <span class="text-sm block mt-2 text-red-500" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mb-6">
                                <label class="block">
                                    <span class="text-sm mb-2 font-semibold">{{ __('Category') }}</span>

                                    <select name="category_id" id="category_id" class="form-select mt-1 block w-full">
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </label>

                                @error('category_id')
                                    <span class="text-sm block mt-2 text-red-500" role="alert">
                                        {{ $message }}
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="flex justify-end items-center">
                        <a href="{{ route('home') }}" class="btn bg-white text-gray-800 hover:bg-indigo-100 hover:text-gray-800 focus:bg-indigo-100 focus:text-gray-800" data-dismiss="modal">Cancel</a>

                        <button class="btn ml-3" type="submit">Add book</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
