require('./bootstrap');

window.Vue = require('vue');

Vue.component('books', require('./components/List.vue').default);

const app = new Vue({
    el: '#app',
});
